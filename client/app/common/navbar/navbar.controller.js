import firebaseui from 'firebaseui';
import * as firebase from 'firebase';

// FirebaseUI config.
const uiConfig = {
  callbacks: {
    signInSuccessWithAuthResult: function(authResult, redirectUrl) {
      // User successfully signed in.
      // Return type determines whether we continue the redirect automatically
      // or whether we leave that to developer to handle.
      return false;
    }
  },
  credentialHelper: firebaseui.auth.CredentialHelper.NONE,
  signInFlow: 'popup',
  signInOptions: [
    firebase.auth.EmailAuthProvider.PROVIDER_ID
  ],
  // Terms of service url.
  tosUrl: '<your-tos-url>'
};

class NavbarController {
  constructor($scope, $timeout) {
    this.$scope = $scope;
    this.$timeout = $timeout;
    // Initialize the FirebaseUI Widget using Firebase.
    this.ui = (firebaseui.auth.AuthUI.getInstance()) ? firebaseui.auth.AuthUI.getInstance() : new firebaseui.auth.AuthUI(firebase.auth());
    this.name = 'navbar';
    this.authState = false;
    this.initApp();
  }

  initApp() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log('User is signed in');
        this.authState = true;
        this.$scope.$apply();
        var providerData = user.providerData;
        user.getIdToken().then((accessToken) => {
          console.log('accessToken', accessToken);
          console.log('user.uid', user.uid);
          console.log('user.emailVerified', user.emailVerified);
          console.log('user.displayName', user.displayName);
          console.log('user.email', user.email);
        });
      } else {
        console.log('User is not logged');
        this.authState = false;
        this.$scope.$apply();
      }
    }, function(error) {
      console.log(error);
    });
  }

  logout () {
    firebase.auth().signOut();
    console.log('Logout');
  }

  login () {
    // The start method will wait until the DOM is loaded.
    this.ui.start('#firebaseui-auth-container', uiConfig);
  }

}

NavbarController.$inject = ['$scope', '$timeout'];

export default NavbarController;
