import template from './app-header.html';
import controller from './app-header.controller';
import './app-header.scss';

let appHeaderComponent = {
  bindings: {},
  template,
  controller
};

export default appHeaderComponent;
