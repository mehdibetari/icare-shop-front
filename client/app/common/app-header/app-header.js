import angular from 'angular';
import uiRouter from 'angular-ui-router';
import appHeaderComponent from './app-header.component';

let appHeaderModule = angular.module('app-header', [
  uiRouter
])

.component('appHeader', appHeaderComponent)

.name;

export default appHeaderModule;
