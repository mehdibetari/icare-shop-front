import angular from 'angular';
import uiRouter from 'angular-ui-router';
import appFooterComponent from './app-Footer.component';

let appFooterModule = angular.module('app-footer', [
  uiRouter
])

.component('appFooter', appFooterComponent)

.name;

export default appFooterModule;
