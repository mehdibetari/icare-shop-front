import template from './app-footer.html';
import controller from './app-footer.controller';
import './app-footer.scss';

let appFooterComponent = {
  bindings: {},
  template,
  controller
};

export default appFooterComponent;
