import angular from 'angular';
import uiRouter from 'angular-ui-router';
import slideshowComponent from './slideshow.component';

let slideshowModule = angular.module('slideshow', [
  uiRouter
])

.component('slideshow', slideshowComponent)

.name;

export default slideshowModule;
