import template from './slideshow.html';
import controller from './slideshow.controller';
import './slideshow.scss';

let slideshowComponent = {
  bindings: {},
  template,
  controller
};

export default slideshowComponent;
