import angular from 'angular';
import AppHeader from './app-header/app-header';
import AppFooter from './app-footer/app-footer';
import Navbar from './navbar/navbar';
import Menu from './menu/menu';
import Slideshow from './slideshow/slideshow';
import User from './user/user';

let commonModule = angular.module('app.common', [
  AppHeader,
  Navbar,
  Slideshow,
  User,
  Menu,
  AppFooter
])

.name;

export default commonModule;
