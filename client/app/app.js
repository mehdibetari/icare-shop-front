import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import firebase from 'firebase/app';
import 'normalize.css';

import 'jquery';
import 'jquery-ui';
import 'jquery.easing';
import 'bootstrap';
import 'smoothscroll-for-websites';
import 'jquery-validation';
import 'icheck';
import 'jquery-placeholder';
import '../../node_modules/jquery.stellar/jquery.stellar';
import '../../node_modules/jquery-touchswipe/jquery.touchSwipe';
import 'shufflejs';
import 'lightgallery';
import 'owl.carousel';
// import './assets/js/plugins/masterslider.min';
import './assets/js/scripts.js';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyBMhJhafSaZC5T0c5UTWojhZXnY5TKBaBg",
  authDomain: "icare-9a048.firebaseapp.com",
  databaseURL: "https://icare-9a048.firebaseio.com",
  projectId: "icare-9a048",
  storageBucket: "icare-9a048.appspot.com",
  messagingSenderId: "426434268533"
};
firebase.initializeApp(config);

angular.module('app', [
    uiRouter,
    Common,
    Components
  ])
  .config(($locationProvider) => {
    "ngInject";
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');
  })

  .component('app', AppComponent);
